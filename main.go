package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"
)


func timeHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		currentTime := time.Now()
		Heure := currentTime.Hour()
		Minute := currentTime.Minute()
		fmt.Fprintf(w, "%dh%d", Heure, Minute)
	}
}

func addEntry(author, entry string) {
	f, err := os.OpenFile("base.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	_, err2 := f.WriteString(author + ": " + entry + "\n")
	if err2 != nil {
		log.Fatal(err2)
	}
}

func addHandler(w http.ResponseWriter, req *http.Request) {
	var author string
	var entry string

	switch req.Method {
	case http.MethodPost:
		if err := req.ParseForm(); err != nil {
			fmt.Println("Something went bad")
			fmt.Fprintln(w, "Something went bad")
			return
		}
		if req.PostForm["author"] != nil && req.PostForm["entry"] != nil {
			for key, value := range req.PostForm {
				fmt.Println(key, "=>", value)
				if key == "author" {
					author = value[0]
				} else if key == "entry" {
					entry = value[0]
				}
			}
			addEntry(author, entry)
			fmt.Fprintf(w, "%s: %s\n", author, entry)
			fmt.Printf("%s: %s\n", author, entry)
		} else {
			fmt.Println("Un champ n'est pas renseigné")
			fmt.Fprintln(w, "Un champ n'est pas renseigné")
		}
	}
}

func entriesHandler(w http.ResponseWriter, req *http.Request) {
	switch req.Method {
	case http.MethodGet:
		content, err := ioutil.ReadFile("base.txt")
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println(string(content))
		fmt.Fprintf(w, string(content))
	}
}
func main() {
	http.HandleFunc("/", timeHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/entries", entriesHandler)
	http.ListenAndServe(":9000", nil)
}
